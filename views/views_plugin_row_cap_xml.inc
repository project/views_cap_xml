<?php

class views_plugin_row_cap_xml extends views_plugin_row {

  var $row_index;
  var $element_tree;

  function option_definition() {
    return array(
      'contentDescription' => array('default' => ''),
      'alert' => array(
        'identifier' => array('default' => ''),
        'sender' => array('default' => ''),
        'sent' => array('default' => ''),
        'status' => array('default' => ''),
        'msgType' => array('default' => ''),
        'scope' => array('default' => ''),
        'code' => array('default' => ''),
        'incidents' => array('default' => ''),
        'info' => array(
          '_parameters' => array(
            'FireDangerClass' => array(
              'value' => array('default' => '1'),
            ),
            'Status' => array(
              'value' => array('default' => ''),
            ),
            'Fireground' => array(
              'value' => array('default' => ''),
            ),
            'AlertLevel' => array(
              'value' => array('default' => ''),
            ),
            'IncidentType' => array(
              'value' => array('default' => ''),
            ),
            'CouncilArea' => array(
              'value' => array('default' => 'Not Available'),
            ),
          ),
          'language' => array('default' => ''),
          'category' => array('default' => ''),
          'event' => array('default' => ''),
          'responseType' => array('default' => ''),
          'urgency' => array('default' => ''),
          'severity' => array('default' => ''),
          'certainty' => array('default' => ''),
          'eventCode' => array(
            'valueName' => array('default' => ''),
            'value' => array('default' => ''),
          ),
          'effective' => array('default' => ''),
          'expires' => array('default' => ''),
          'senderName' => array('default' => ''),
          'headline' => array('default' => ''),
          'description' => array('default' => ''),
          'instruction' => array('default' => ''),
          'web' => array('default' => ''),
          'contact' => array('default' => ''),
          'resource' => array(
            'resourceDesc' => array('default' => 'map'),
            'mimeType' => array('default' => 'text/html'),
            'uri' => array('default' => ''),
          ),
          'area' => array(
            'areaDesc' => array('default' => ''),
            'polygon' => array('default' => ''),
            'circle' => array('default' => ''),
            'geocode' => array(
              'valueName' => array('default' => ''),
              'value' => array('default' => ''),
            ),
          ),
        ),
      ),
    );
  }

  function build_option_definition_tree(&$options, $xml_el_name, $detail, $option_key = '') {

    if(isset($detail['sub'])) {
      foreach($detail['sub'] as $sub_xml_el_name => $sub_detail) {
        $this->build_option_definition_tree($options, $sub_xml_el_name,$sub_detail,$option_key.'_'.$sub_xml_el_name);
      }
      unset($options[$xml_el_name]);
    }
    else {
      $options[$option_key] = array('default' => '');
    }
  }

  function build_options_form_tree($xml_el_name, $detail, $options) {

    $initial_labels = array('' => t('- None -'));
    if (!empty($detail['default'])) {
      $initial_labels['_default'] = '"'.$detail['default'].'"';
    }

    $view_fields_labels = $this->display->handler->get_field_labels();
    $view_fields_labels = array_merge($initial_labels, $view_fields_labels);

    $el = array();

    $el[$xml_el_name] = array(
      '#type' => 'select',
      '#title' => $xml_el_name,
      '#options' => $view_fields_labels,
      '#default_value' => isset($options[$xml_el_name])?$options[$xml_el_name]:'',
      '#required' => FALSE,
    );

    if(is_array($detail) && !isset($detail['default'])) {

      $el[$xml_el_name] = array(
        '#type' => 'fieldset',
        '#title' => $xml_el_name,
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );

      foreach($detail as $sub_xml_el_name => $sub_detail) {
        $el[$xml_el_name] += $this->build_options_form_tree($sub_xml_el_name, $sub_detail, $options[$xml_el_name]);
      }

    }

    return $el;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    foreach($this->option_definition() as $xml_el_name => $detail) {
      $form += $this->build_options_form_tree($xml_el_name, $detail, $this->options);
    }
  }

  function validate() {
    $errors = parent::validate();
    return $errors;
  }



  function build_element_tree($xml_el_name, $detail, $options) {

    if(is_string($options[$xml_el_name]) && !empty($options[$xml_el_name])) {

      if($options[$xml_el_name] == '_default') {
        $value = $detail['default'];
      } else {
        $value = $this->get_field($this->row_index, $options[$xml_el_name]);
      }
    }

    $el = array();
    if(!empty($value)) {
      $el = array(
        'key' => $xml_el_name,
        'value' => $value,
      );
    }

    if(is_array($detail) && !isset($detail['default'])) {

      $values = array();
      foreach($detail as $sub_xml_el_name => $sub_detail) {
        $sub_value = $this->build_element_tree($sub_xml_el_name, $sub_detail, $options[$xml_el_name]);
        if (!empty($sub_value)) {

          //Special case for the _parameters part of the array
          //It's formatted differently so needs special processing here
          if ($sub_xml_el_name == '_parameters') {
            
            foreach ($sub_value['value'] as $param) {

              $option_value = $options[$xml_el_name]['_parameters'][$param['key']]['value'];
              if ($option_value == '_default') {
                $value = $param['value'][0]['value'];
              }
              else {
                $value = $this->get_field($this->row_index, $option_value);
              }

              $param_el = array(
                'key' => 'parameter',
                'value' => array(
                  'valueName' => $param['key'],
                  'value' => $value
                )
              );
              $values[] = $param_el;
            }
          }
          else {
            $values[] = $sub_value;
          }
        }
      }

      if(!empty($values)) {
        $el = array('key'=> $xml_el_name, 'value' => $values);
      }
    }

    return $el;

  }


  function render($row) {
    static $row_index;
    if (!isset($row_index)) {
      $row_index = 0;
    }
    $this->row_index = $row_index;

    // Create the RSS item object.
    $contentObject = new stdClass();

    $option_defn = $this->option_definition();
    $cap_elements = array();
    foreach ($option_defn['alert'] as $xml_el_name => $detail) {
      $el = $this->build_element_tree($xml_el_name, $detail, $this->options['alert']);
      if(!empty($el)) {
        $cap_elements[] = $el;
      }
    }

    $contentObject->elements[] = array(
      'key' => 'contentObject',
      'value' => array(

        array('key'=>'contentDescription', 'value' => $this->get_field($this->row_index, $this->options['contentDescription'])),

        array(
          'key'=> 'xmlContent',
          'value' => array(
            array(
              'key' => 'embeddedXMLContent',
              'value' => array(
                array(
                  'key' => 'alert',
                  'value' => $cap_elements,
                  'attributes' => array('xmlns' => 'urn:oasis:names:tc:emergency:1.2')),
              ),              
            ),
          ),
        ),
      )
    );


    $row_index++;

    return theme($this->theme_functions(),
      array(
        'view' => $this->view,
        'options' => $this->options,
        'contentObject' => $contentObject,
        'field_alias' => isset($this->field_alias) ? $this->field_alias : '',
      )
    );
  }

  /**
   * Retrieves a views field value from the style plugin.
   *
   * @param $index
   *   The index count of the row as expected by views_plugin_style::get_field().
   * @param $field_id
   *   The ID assigned to the required field in the display.
   */
  function get_field($index, $field_id) {

    if (empty($this->view->style_plugin) || !is_object($this->view->style_plugin) || empty($field_id)) {
      return '';
    }
    return $this->view->style_plugin->get_field($index, $field_id);
  }
}
